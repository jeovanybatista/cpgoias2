---
marp: true
theme: gaia
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

# Monitoramento de Aplicações com ferramentas **OpenSource**
Por: Jeovany Batista

---
# **Introdução**

## Monitoramento de aplicações
  - Necessidades;
  - Evolução de tecnologia e processos;
  - Ferramentas OpenSource:
    - Prometheus
    - Grafana 

---
# **Prometheus**

> #### *Sistema de Monitoramento projetado principalmente para monitoramento de aplicações, microserviços e coleta de dados de série temporal de trabalhos instrumentados.*

---
# **Grafana**

> #### *O Grafana é uma plataforma para visualização e análise de dados por meio de gráficos.*

---
# **Instalação**

Para subir nosso ambiente de monitoramento precisaremos realizar a instalação de duas ferramentas:

* Docker;
* Git;

---
# **Docker**
```bash
$ sudo apt-get update
$ sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
$ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
$ echo "deb [arch=$(dpkg --print-architecture) \ 
signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
https://download.docker.com/linux/debian \
$(lsb_release -cs) stable"
$ sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

---
# **Docker-compose**

```sh
$ sudo curl -L "https://github.com/docker/compose/releases/
download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \ 
-o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ docker-compose --version
```
:warning: **Atenção!**: Se o comando docker-compose falhar, verifique a permissão para execução ou o caminho do binário. Uma possibilidade é você criar um link simbólico para o diretório /usr/bin.
```sh
$ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

---
# **Git**
#### Instalando
```shell
$ sudo apt update
$ sudo apt install git
```
##### Testando
```shell
$ git --version
```
##### Clonando o repositório
```sh
$ git clone https://gitlab.com/jeovanybatista/challenger_4linux.git
```

---
# **Prometheus e Grafana**

* Diretório persistido
```shell
$ sudo mkdir -p /etc/prometheus
```
**Nota!**: você pode criar o diretório em qualquer local do seu sistema, porém deverá se lembrar de alterar o arquivo YAML para que o volume persistido seja aquele que você definiu.

* Vamos criar o arquivo de configuração:
```shell
sudo vim /etc/prometheus/prometheus.yml
```

---
###### Arquivo prometheus.yml
<style scoped>
pre {
  font-size: 68%;
}
</style>
```yaml
global:
  scrape_interval:  10s
  evaluation_interval: 10s

rule_files:
   # - "first_rules.yml"
   # - "second_rules.yml"

scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['localhost:9090']
  
  - job_name: 'NodeExporter'
    static_configs:
      - targets: ['172.22.0.4:9100']

  - job_name: 'DockerExporter'
    static_configs:
      - targets: ['172.22.0.2:9417']
```
---
# **Instalando Prometheus**
Vamos dar uma olhada no que temos dentro do nosso arquivo docker-compose.yml?


 ###### Prometheus:
<style scoped>
pre {
  font-size: 58%;
}
</style>

```yaml
version: '2'

services:
  prometheus:
    container_name: prometheus
    hostname: prometheus
    image: prom/prometheus:latest
    ports:
      - 9090:9090
    volumes:
      - /etc/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
    restart: unless-stopped
```

---
 ###### Node-Exporter:
```yaml
  node-exporter:
    container_name: node-exporter
    hostname: node-exporter
    image: prom/node-exporter:latest
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command: 
      - '--path.procfs=/host/proc' 
      - '--path.sysfs=/host/sys'
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - 9100:9100
    restart: unless-stopped
```

---
 ###### Docker-Exporter:
<style scoped>
pre {
  font-size: 68%;
}
</style>
```yaml
  docker-exporter:
    container_name: docker-exporter
    hostname: docker-exporter
    image: prometheusnet/docker_exporter:latest
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    ports:
      - 9417:9417
    restart: always
```

---     
 ###### Grafana:
<style scoped>
pre {
  font-size: 68%;
}
</style>
```yaml
  grafana:
    image: grafana/grafana
    container_name: grafana
    volumes:
      - grafana-storage:/var/lib/grafana
    ports:
      - 3000:3000
    networks:
      - "monitoring-network"
    depends_on:
      - prometheus

volumes:
  grafana-storage:

```
---
# **Vamos à instalação dos ambientes!**

```shell
$ cd challenger_4linux
$ cd prometheus
$ docker-compose up -d
```
#### Pronto, nossas aplicações estão **NO AR!**
---

# **Prometheus Interface**

![width:1080px](Prometheus_Interface.png)

---
# **Prometheus Targets**

![width:985px](Prometheus_Targets.png)

---
# **Grafana Dashbords**

![width:1100px](Grafana_Dash.png)

---
<!-- _class: lead -->
# **Muito obrigado!**